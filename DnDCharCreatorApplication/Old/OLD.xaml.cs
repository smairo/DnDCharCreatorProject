﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DnDCharCreatorApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //DELETE BEFORE REL01:
        public string debug, debug2, debug3;
        //END DELETE

        private int ASRemaining, AScorecase;

        // Kyseenalainen:
        private int raceSTR, raceDEX, raceCON, raceINT, raceWIS, raceCHA,
            vraceSTR, vraceDEX, vraceCON, vraceINT, vraceWIS, vraceCHA = 0;

        public ObservableCollection<string> Subraces;


        public MainWindow()
        {
            InitializeComponent();
        }

        // BUG1: Double / quicktapping causes Remaining abilityscore to reduce / grow too much
        #region AbilityScore_Controls

        #region Minus
        private void btnMSTR_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtSTR.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtSTR.Text = "8";
                    break;

                case 14:
                    txtSTR.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtSTR.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtSTR.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        private void btnMDEX_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtDEX.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtSTR.Text = "8";
                    break;

                case 14:
                    txtDEX.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtDEX.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtDEX.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        private void btnMCON_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtCON.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtCON.Text = "8";
                    break;

                case 14:
                    txtCON.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtCON.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtCON.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        private void btnMINT_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtINT.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtINT.Text = "8";
                    break;

                case 14:
                    txtINT.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtINT.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtINT.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        private void btnMWIS_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtWIS.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtWIS.Text = "8";
                    break;

                case 14:
                    txtWIS.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtWIS.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtWIS.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        private void btnMCHA_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtCHA.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtCHA.Text = "8";
                    break;

                case 14:
                    txtCHA.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtCHA.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtCHA.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }


        #endregion

        #region Plus
        private void btnPSTR_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtSTR.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                //case 8:
                //    txtSTR.Text = "8";
                //    break;

                case 13:
                    if (ASRemaining > 1)
                    {
                        txtSTR.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtSTR.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtSTR.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtSTR.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }
        private void btnPDEX_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtDEX.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                //case 8:
                //    txtSTR.Text = "8";
                //    break;

                case 13:
                    if (ASRemaining > 1)
                    {
                        txtDEX.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtDEX.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtDEX.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtDEX.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }



        private void btnPCON_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtCON.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                //case 8:
                //    txtSTR.Text = "8";
                //    break;

                case 13:
                    if (ASRemaining > 1)
                    {
                        txtCON.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtCON.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtCON.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtCON.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }



        private void btnPINT_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtINT.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                //case 8:
                //    txtSTR.Text = "8";
                //    break;

                case 13:
                    if (ASRemaining > 1)
                    {
                        txtINT.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtINT.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtINT.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtINT.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }

        private void btnPWIS_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtWIS.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                //case 8:
                //    txtSTR.Text = "8";
                //    break;

                case 13:
                    if (ASRemaining > 1)
                    {
                        txtWIS.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtWIS.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtWIS.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtWIS.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }



        private void btnPCHA_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtCHA.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                //case 8:
                //    txtSTR.Text = "8";
                //    break;

                case 13:
                    if (ASRemaining > 1)
                    {
                        txtCHA.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtCHA.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtCHA.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtCHA.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }
        #endregion

        #region TextChanged event handler
        private void txtSTR_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFSTR.Text = (int.Parse(txtSTR.Text) + raceSTR + vraceSTR).ToString();
            }
            catch (Exception)
            {
            }
        }
        private void txtDEX_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFDEX.Text = (int.Parse(txtDEX.Text) + raceDEX + vraceDEX).ToString();
            }
            catch (Exception)
            {
            }
        }
        private void txtCON_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFCON.Text = (int.Parse(txtCON.Text) + raceCON + vraceCON).ToString();
            }
            catch (Exception)
            {
            }

        }
        private void txtINT_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFINT.Text = (int.Parse(txtINT.Text) + raceINT + vraceINT).ToString();
            }
            catch (Exception)
            {
            }
        }
        private void txtWIS_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFWIS.Text = (int.Parse(txtWIS.Text) + raceWIS + vraceWIS).ToString();
            }
            catch (Exception)
            {
            }
        }
        private void txtCHA_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFCHA.Text = (int.Parse(txtCHA.Text) + raceCHA + vraceCHA).ToString();
            }
            catch (Exception)
            {
            }
        }
        #endregion

        private void btnResetAS_Click(object sender, RoutedEventArgs e)
        {
            txtSTR.Text = "8";
            txtDEX.Text = "8";
            txtCON.Text = "8";
            txtINT.Text = "8";
            txtWIS.Text = "8";
            txtCHA.Text = "8";
            txtPointsleft.Text = "27";
        }
        #endregion

        private void cbRace_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnVariant.Visibility = Visibility.Hidden;
            cbSubrace.Visibility = Visibility.Visible;

            resetMostOfTheUi();
            Subraces = new ObservableCollection<string>();

            ComboBoxItem racevalue = (ComboBoxItem)cbRace.SelectedItem;
            string race = racevalue.Content.ToString();

            switch (race)
            {
                case "Dwarf":
                    raceCON = 2;
                    txtFCON.Text = (int.Parse(txtFCON.Text) + raceCON).ToString();

                    txtSpeed.Text = "25";
                    txtLanguages.Text = "Common, Dwarvish, ";
                    txtExtra.Text = "Darkvision 60ft, ";
                    txtExtra.Text = txtExtra.Text + "Dwarven Resilence, ";
                    txtExtra.Text = txtExtra.Text + "Dwarven Combat training";

                    cbSubrace.ItemsSource = new string[] { "Choose!", "Hill Dwarf", "Mountain Dwarf" };
                    cbSubrace.SelectedIndex = 0;
                    break;

                case "Elf":
                    raceDEX = 2;
                    txtFDEX.Text = (int.Parse(txtFDEX.Text) + raceDEX).ToString();
                    txtSpeed.Text = "30";
                    txtExtra.Text = "Darkvision 60ft, ";
                    txtExtra.Text = txtExtra.Text + "Fey Ancestry, ";
                    txtExtra.Text = txtExtra.Text + "Trance";
                    Perception.IsChecked = true;
                    txtLanguages.Text = txtLanguages.Text + ", Elvish";

                    cbSubrace.ItemsSource = new string[] { "Choose!", "High Elf", "Wood Elf", "Drow" };
                    cbSubrace.SelectedIndex = 0;
                    break;

                case "Halfling":
                    raceDEX = 2;
                    txtFDEX.Text = (int.Parse(txtFDEX.Text) + raceDEX).ToString();
                    txtSpeed.Text = "25";
                    txtExtra.Text = "Lucky, Brave, Halfling Nimbleness, Size: Small";
                    txtLanguages.Text = "Common, Halfling";

                    cbSubrace.ItemsSource = new string[] { "Choose!", "Lightfoot", "Stout" };
                    cbSubrace.SelectedIndex = 0;
                    break;

                case "Human":
                    raceCHA = 1;
                    raceCON = 1;
                    raceDEX = 1;
                    raceINT = 1;
                    raceSTR = 1;
                    raceWIS = 1;
                    txtFCHA.Text = (int.Parse(txtFCHA.Text) + raceCHA).ToString();
                    txtFCON.Text = (int.Parse(txtFCON.Text) + raceCON).ToString();
                    txtFDEX.Text = (int.Parse(txtFDEX.Text) + raceDEX).ToString();
                    txtFINT.Text = (int.Parse(txtFINT.Text) + raceINT).ToString();
                    txtFSTR.Text = (int.Parse(txtFSTR.Text) + raceSTR).ToString();
                    txtFWIS.Text = (int.Parse(txtFWIS.Text) + raceWIS).ToString();

                    txtSpeed.Text = "30";

                    txtLanguages.Text = "Common, EXTRA!";

                    btnVariant.Visibility = Visibility.Visible;
                    cbSubrace.Visibility = Visibility.Hidden;

                    break;

                case "Dragonborn":
                    break;

                case "Gnome":
                    break;

                case "Half-elf":
                    break;

                case "Half-orc":
                    break;

                case "Tiefling":
                    break;

                case "Aasimar":
                    break;

                case "Eladrin":
                    break;

                case "Changeling":
                    break;

                case "Shifter":
                    break;

                case "Warforged":
                    break;

                case "Aarakocra":
                    break;

                case "Genasi":
                    break;

                case "Goliath":
                    break;

                case "Minotaur":
                    break;

            }
        }

        // BUG2: Subrace spams to txtExtra if you switch subraces constantly
        private void cbSubrace_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            vraceSTR = 0;
            vraceDEX = 0;
            vraceCON = 0;
            vraceINT = 0;
            vraceWIS = 0;
            vraceCHA = 0;

            txtFSTR.Text = (int.Parse(txtSTR.Text) + raceSTR).ToString();
            txtFDEX.Text = (int.Parse(txtDEX.Text) + raceDEX).ToString();
            txtFCON.Text = (int.Parse(txtCON.Text) + raceCON).ToString();
            txtFINT.Text = (int.Parse(txtINT.Text) + raceINT).ToString();
            txtFWIS.Text = (int.Parse(txtWIS.Text) + raceWIS).ToString();
            txtFCHA.Text = (int.Parse(txtCHA.Text) + raceCHA).ToString();

            string subrace = (string)(cbSubrace.SelectedItem);


            switch (subrace)
            {
                case "Hill Dwarf":
                    vraceWIS = 1;
                    txtExtra.Text = txtExtra.Text + ", Dwarven toughness (Hill dwarf)";
                    txtFWIS.Text = (int.Parse(txtFWIS.Text) + vraceWIS).ToString();
                    break;

                case "Mountain Dwarf":
                    vraceSTR = 2;
                    txtFSTR.Text = (int.Parse(txtFSTR.Text) + vraceSTR).ToString();
                    cbArmor.ItemsSource = new string[] { "Choose!", "Light", "Medium" };
                    break;

                case "High Elf":
                    vraceINT = 1;
                    txtFINT.Text = (int.Parse(txtFINT.Text) + vraceINT).ToString();
                    txtProfweap.Text = "Longsword, shortsword, shortbow, and longbow.";
                    txtLanguages.Text = txtLanguages.Text + ", EXTRA!";
                    txtSpells.Text = "Any one Wizard cantrip while Intelligence as spellcasting ability for it (subrace feat)";
                    break;

                case "Wood Elf":
                    vraceWIS = 1;
                    txtFWIS.Text = (int.Parse(txtFWIS.Text) + vraceWIS).ToString();
                    txtProfweap.Text = "Longsword, shortsword, shortbow, and longbow.";
                    txtLanguages.Text = "Common, Elvish";
                    txtSpeed.Text = "35";
                    txtExtra.Text = txtExtra.Text + ", Mask of the wild (Wood-elf)";
                    break;

                case "Drow":
                    vraceCHA = 1;
                    txtFCHA.Text = (int.Parse(txtFCHA.Text) + vraceCHA).ToString();
                    txtProfweap.Text = "Rapiers, shortswords, and hand crossbows.";
                    txtLanguages.Text = "Common, Elvish";
                    txtExtra.Text = txtExtra.Text + ", Darkvision 120ft (Drow), Sunlight Sensivity (Drow), Drow Magic (Drow)";
                    break;

                case "Lightfoot":
                    vraceCHA = 1;
                    txtFCHA.Text = (int.Parse(txtFCHA.Text) + vraceCHA).ToString();
                    txtExtra.Text = txtExtra.Text + ", Naturally stealthy (Lightfoot)";
                    break;

                case "Stout":
                    vraceCON = 1;
                    txtFCON.Text = (int.Parse(txtFCON.Text) + vraceCON).ToString();
                    txtExtra.Text = txtExtra.Text + ", Stout Resilence (Stout)";
                    break;

                case "Variant":
                    break;

                case "Tiefling":
                    break;

                case "Aasimar":
                    break;

                case "Eladrin":
                    break;

                case "Changeling":
                    break;

                case "Shifter":
                    break;

                case "Warforged":
                    break;

                case "Aarakocra":
                    break;

                case "Genasi":
                    break;

                case "Goliath":
                    break;

                case "Minotaur":
                    break;

            }

        }

        #region Race and subrace logic


        private void resetMostOfTheUi()
        {
            raceSTR = 0;
            raceDEX = 0;
            raceCON = 0;
            raceINT = 0;
            raceWIS = 0;
            raceCHA = 0;

            vraceSTR = 0;
            vraceDEX = 0;
            vraceCON = 0;
            vraceINT = 0;
            vraceWIS = 0;
            vraceCHA = 0;

            txtFSTR.Text = txtSTR.Text;
            txtDEX.Text = txtDEX.Text;
            txtFCON.Text = txtCON.Text;
            txtFINT.Text = txtINT.Text;
            txtFWIS.Text = txtWIS.Text;
            txtFCHA.Text = txtCHA.Text;

            cbSubrace.ItemsSource = new string[] { "" };
            //cbSubrace.SelectedIndex = -1;

            cbClass.SelectedIndex = -1;

            txtLanguages.Text = "Common";
            rbCHA.IsChecked = false;
            rbCON.IsChecked = false;
            rbDEX.IsChecked = false;
            rbINT.IsChecked = false;
            rbSTR.IsChecked = false;
            rbWIS.IsChecked = false;

            Athletics.IsChecked = false;
            Acrobatics.IsChecked = false;
            Sleight.IsChecked = false;
            Stealth.IsChecked = false;
            Arcana.IsChecked = false;
            History.IsChecked = false;
            Investigation.IsChecked = false;
            Nature.IsChecked = false;
            Religion.IsChecked = false;
            Animal.IsChecked = false;
            Insight.IsChecked = false;
            Medicine.IsChecked = false;
            Perception.IsChecked = false;
            Survival.IsChecked = false;
            Deception.IsChecked = false;
            Intimidation.IsChecked = false;
            Performance.IsChecked = false;

            cbArmor.ItemsSource = new string[] { "" };
            txtProfweap.Text = "";
            txtSpells.Text = "";

            cbSp1.ItemsSource = new string[] { "" };
            cbSp2.ItemsSource = new string[] { "" };
            cbSp3.ItemsSource = new string[] { "" };
            cbSp4.ItemsSource = new string[] { "" };
            cbSp5.ItemsSource = new string[] { "" };
            cbSp6.ItemsSource = new string[] { "" };
            cbSp7.ItemsSource = new string[] { "" };
            cbSp8.ItemsSource = new string[] { "" };
            cbSp9.ItemsSource = new string[] { "" };
            cbSp10.ItemsSource = new string[] { "" };
            cbSp11.ItemsSource = new string[] { "" };
            cbSp12.ItemsSource = new string[] { "" };

            txtExtra.Text = "";

        }

        private void btnVariant_Click(object sender, RoutedEventArgs e)
        {
            raceCHA = 0;
            raceCON = 0;
            raceDEX = 0;
            raceINT = 0;
            raceSTR = 0;
            raceWIS = 0;
            txtFCHA.Text = txtCHA.Text;
            txtFCON.Text = txtCHA.Text;
            txtFDEX.Text = txtCHA.Text;
            txtFINT.Text = txtCHA.Text;
            txtFSTR.Text = txtCHA.Text;
            txtFWIS.Text = txtCHA.Text;

            cbSp1.ItemsSource = new string[] { "Choose +1 to ability", "STR", "DEX", "CON", "INT", "WIS", "CHA" };
            cbSp1.SelectedIndex = 0;

            cbSp2.IsEnabled = false;
        }

        private void cbSp1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (btnVariant.Visibility == Visibility.Visible)
            {
                string selabi = (string)(cbSp1.SelectedItem);

                switch (selabi)
                {
                    case "STR":
                        vraceSTR = 1;
                        txtFSTR.Text = (int.Parse(txtFSTR.Text) + 1).ToString();
                        break;
                    case "DEX":
                        vraceDEX = 1;
                        txtFDEX.Text = (int.Parse(txtFDEX.Text) + 1).ToString();
                        break;
                    case "CON":
                        vraceCON = 1;
                        txtFCON.Text = (int.Parse(txtFCON.Text) + 1).ToString();
                        break;
                    case "INT":
                        vraceINT = 1;
                        txtFINT.Text = (int.Parse(txtFINT.Text) + 1).ToString();
                        break;
                    case "WIS":
                        vraceWIS = 1;
                        txtFWIS.Text = (int.Parse(txtFWIS.Text) + 1).ToString();
                        break;
                    case "CHA":
                        vraceCHA = 1;
                        txtFCHA.Text = (int.Parse(txtFCHA.Text) + 1).ToString();
                        break;
                }


                List<string> abilist = new List<string>();
                abilist.Add("Choose +1 to ability");
                string[] growabi;
                string[] abilities = { "STR", "DEX", "CON", "INT", "WIS", "CHA" };
                foreach (string value in abilities)
                {
                    if (value != selabi)
                    {
                        abilist.Add(value);
                    }
                }
                growabi = abilist.ToArray();

                cbSp2.ItemsSource = growabi;

                cbSp2.IsEnabled = true;
                //cbSp1.IsEnabled = false;
            }
        }

        private void cbSp2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbSp1.IsEnabled = false;
            if (btnVariant.Visibility == Visibility.Visible)
            {
                string selabi = (string)(cbSp2.SelectedItem);

                switch (selabi)
                {
                    case "STR":
                        vraceSTR = 1;
                        txtFSTR.Text = (int.Parse(txtFSTR.Text) + 1).ToString();
                        cbSp2.IsEnabled = false;
                        btnVariant.IsEnabled = false;
                        break;
                    case "DEX":
                        vraceDEX = 1;
                        txtFDEX.Text = (int.Parse(txtFDEX.Text) + 1).ToString();
                        cbSp2.IsEnabled = false;
                        btnVariant.IsEnabled = false;
                        break;
                    case "CON":
                        vraceCON = 1;
                        txtFCON.Text = (int.Parse(txtFCON.Text) + 1).ToString();
                        cbSp2.IsEnabled = false;
                        btnVariant.IsEnabled = false;
                        break;
                    case "INT":
                        vraceINT = 1;
                        txtFINT.Text = (int.Parse(txtFINT.Text) + 1).ToString();
                        cbSp2.IsEnabled = false;
                        btnVariant.IsEnabled = false;
                        break;
                    case "WIS":
                        vraceWIS = 1;
                        txtFWIS.Text = (int.Parse(txtFWIS.Text) + 1).ToString();
                        cbSp2.IsEnabled = false;
                        btnVariant.IsEnabled = false;
                        break;
                    case "CHA":
                        vraceCHA = 1;
                        txtFCHA.Text = (int.Parse(txtFCHA.Text) + 1).ToString();
                        cbSp2.IsEnabled = false;
                        btnVariant.IsEnabled = false;
                        break;
                }
                //cbSp2.IsEnabled = true;
            }
        }

        #endregion

        private void btnHardreset_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow();
            window.Show();
            this.Close();
        }
    }
}
