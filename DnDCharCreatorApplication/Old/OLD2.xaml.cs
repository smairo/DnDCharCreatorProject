﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DnDCharCreatorApplication.Wpfs
{
    /// <summary>
    /// Interaction logic for NewChar.xaml
    /// </summary>
    public partial class NewChar : Window
    {
        //DELETE BEFORE REL01:
        public string debug, debug2, debug3;
        //END DELETE

        // Private variables
        private int ASRemaining, AScorecase;
        private int raceSTR, raceDEX, raceCON, raceINT, raceWIS, raceCHA,
            vraceSTR, vraceDEX, vraceCON, vraceINT, vraceWIS, vraceCHA = 0;

        // Public variables
        public ObservableCollection<string> Subraces;

        // Window constructor
        public NewChar()
        {
            InitializeComponent();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            // IF page is closed
        }

        public Char Character { get; set; }

        #region AbilityScore Controls
        // Minus logic
        #region Minus
        private void btnMSTR_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtSTR.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtSTR.Text = "8";
                    break;

                case 14:
                    txtSTR.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtSTR.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtSTR.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        private void btnMDEX_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtDEX.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtDEX.Text = "8";
                    break;

                case 14:
                    txtDEX.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtDEX.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtDEX.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        private void btnMCON_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtCON.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtCON.Text = "8";
                    break;

                case 14:
                    txtCON.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtCON.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtCON.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        private void btnMINT_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtINT.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtINT.Text = "8";
                    break;

                case 14:
                    txtINT.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtINT.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtINT.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        private void btnMWIS_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtWIS.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtWIS.Text = "8";
                    break;

                case 14:
                    txtWIS.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtWIS.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtWIS.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        private void txtFSTR_TextChanged(object sender, TextChangedEventArgs e)
        {
            int tmp = int.Parse(txtFSTR.Text);

            try
            {
                switch (tmp)
                {
                    case 8:
                    case 9:
                        lblSTRMod.Content = "-1";
                        break;

                    case 10:
                    case 11:
                        lblSTRMod.Content = " 0";
                        break;

                    case 12:
                    case 13:
                        lblSTRMod.Content = "+1";
                        break;

                    case 14:
                    case 15:
                        lblSTRMod.Content = "+2";
                        break;

                    case 16:
                    case 17:
                        lblSTRMod.Content = "+3";
                        break;

                    case 18:
                    case 19:
                        lblSTRMod.Content = "+4";
                        break;

                    case 20:
                        lblSTRMod.Content = "+5";
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void txtFDEX_TextChanged(object sender, TextChangedEventArgs e)
        {
            int tmp = int.Parse(txtFDEX.Text);
            try
            {
                switch (tmp)
                {
                    case 8:
                    case 9:
                        lblDEXMod.Content = "-1";
                        break;

                    case 10:
                    case 11:
                        lblDEXMod.Content = " 0";
                        break;

                    case 12:
                    case 13:
                        lblDEXMod.Content = "+1";
                        break;

                    case 14:
                    case 15:
                        lblDEXMod.Content = "+2";
                        break;

                    case 16:
                    case 17:
                        lblDEXMod.Content = "+3";
                        break;

                    case 18:
                    case 19:
                        lblDEXMod.Content = "+4";
                        break;

                    case 20:
                        lblDEXMod.Content = "+5";
                        break;
                }


            }
            catch (Exception)
            {

            }
        }

        private void txtFCON_TextChanged(object sender, TextChangedEventArgs e)
        {
            int tmp = int.Parse(txtFCON.Text);

            try
            {
                switch (tmp)
                {
                    case 8:
                    case 9:
                        lblCONMod.Content = "-1";
                        break;

                    case 10:
                    case 11:
                        lblCONMod.Content = " 0";
                        break;

                    case 12:
                    case 13:
                        lblCONMod.Content = "+1";
                        break;

                    case 14:
                    case 15:
                        lblCONMod.Content = "+2";
                        break;

                    case 16:
                    case 17:
                        lblCONMod.Content = "+3";
                        break;

                    case 18:
                    case 19:
                        lblCONMod.Content = "+4";
                        break;

                    case 20:
                        lblCONMod.Content = "+5";
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void txtFINT_TextChanged(object sender, TextChangedEventArgs e)
        {
            int tmp = int.Parse(txtFINT.Text);

            try
            {
                switch (tmp)
                {
                    case 8:
                    case 9:
                        lblINTMod.Content = "-1";
                        break;

                    case 10:
                    case 11:
                        lblINTMod.Content = " 0";
                        break;

                    case 12:
                    case 13:
                        lblINTMod.Content = "+1";
                        break;

                    case 14:
                    case 15:
                        lblINTMod.Content = "+2";
                        break;

                    case 16:
                    case 17:
                        lblINTMod.Content = "+3";
                        break;

                    case 18:
                    case 19:
                        lblINTMod.Content = "+4";
                        break;

                    case 20:
                        lblINTMod.Content = "+5";
                        break;
                }

            }
            catch (Exception)
            {
            }
        }

        private void txtFWIS_TextChanged(object sender, TextChangedEventArgs e)
        {
            int tmp = int.Parse(txtFWIS.Text);

            try
            {
                switch (tmp)
                {
                    case 8:
                    case 9:
                        lblWISMod.Content = "-1";
                        break;

                    case 10:
                    case 11:
                        lblWISMod.Content = " 0";
                        break;

                    case 12:
                    case 13:
                        lblWISMod.Content = "+1";
                        break;

                    case 14:
                    case 15:
                        lblWISMod.Content = "+2";
                        break;

                    case 16:
                    case 17:
                        lblWISMod.Content = "+3";
                        break;

                    case 18:
                    case 19:
                        lblWISMod.Content = "+4";
                        break;

                    case 20:
                        lblWISMod.Content = "+5";
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void txtFCHA_TextChanged(object sender, TextChangedEventArgs e)
        {
            int tmp = int.Parse(txtFCHA.Text);
            try
            {
                switch (tmp)
                {
                    case 8:
                    case 9:
                        lblCHAMod.Content = "-1";
                        break;

                    case 10:
                    case 11:
                        lblCHAMod.Content = " 0";
                        break;

                    case 12:
                    case 13:
                        lblCHAMod.Content = "+1";
                        break;

                    case 14:
                    case 15:
                        lblCHAMod.Content = "+2";
                        break;

                    case 16:
                    case 17:
                        lblCHAMod.Content = "+3";
                        break;

                    case 18:
                    case 19:
                        lblCHAMod.Content = "+4";
                        break;

                    case 20:
                        lblCHAMod.Content = "+5";
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnMCHA_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtCHA.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtCHA.Text = "8";
                    break;

                case 14:
                    txtCHA.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtCHA.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtCHA.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        #endregion
        // Plus logic
        #region Plus
        private void btnPSTR_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtSTR.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 13:
                    if (ASRemaining > 1)
                    {
                        txtSTR.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtSTR.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtSTR.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtSTR.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }
        private void btnPDEX_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtDEX.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 13:
                    if (ASRemaining > 1)
                    {
                        txtDEX.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtDEX.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtDEX.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtDEX.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }



        private void btnPCON_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtCON.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 13:
                    if (ASRemaining > 1)
                    {
                        txtCON.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtCON.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtCON.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtCON.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }



        private void btnPINT_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtINT.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 13:
                    if (ASRemaining > 1)
                    {
                        txtINT.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtINT.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtINT.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtINT.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }

        private void btnPWIS_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtWIS.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 13:
                    if (ASRemaining > 1)
                    {
                        txtWIS.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtWIS.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtWIS.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtWIS.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }



        private void btnPCHA_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtCHA.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 13:
                    if (ASRemaining > 1)
                    {
                        txtCHA.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtCHA.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtCHA.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtCHA.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }
        #endregion
        // txt Change logic
        #region TextChanged event handler
        private void txtSTR_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFSTR.Text = (int.Parse(txtSTR.Text) + raceSTR + vraceSTR).ToString();
            }
            catch (Exception)
            {
            }
        }
        private void txtDEX_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFDEX.Text = (int.Parse(txtDEX.Text) + raceDEX + vraceDEX).ToString();
            }
            catch (Exception)
            {
            }
        }
        private void txtCON_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFCON.Text = (int.Parse(txtCON.Text) + raceCON + vraceCON).ToString();
            }
            catch (Exception)
            {
            }

        }
        private void txtINT_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFINT.Text = (int.Parse(txtINT.Text) + raceINT + vraceINT).ToString();
            }
            catch (Exception)
            {
            }
        }
        private void txtWIS_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFWIS.Text = (int.Parse(txtWIS.Text) + raceWIS + vraceWIS).ToString();
            }
            catch (Exception)
            {
            }
        }
        private void txtCHA_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFCHA.Text = (int.Parse(txtCHA.Text) + raceCHA + vraceCHA).ToString();
            }
            catch (Exception)
            {
            }
        }
        #endregion
        #endregion

        #region Comboboxes Logic
        // Class selection
        private void cbClass_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // lblSp 1-13
            for (int i = 1; i < 13; i++)
            {
                Label lb = (Label)FindName("lblSp" + i);
                lb.Content = "";
            }

        }

        // Race selection
        private void cbRace_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // lblSp 15-26
            for (int i = 15; i < 26; i++)
            {
                Label lb = (Label)FindName("lblSp" + i);
                lb.Content = "";
            }

            Subraces = new ObservableCollection<string>();

            ComboBoxItem racevalue = (ComboBoxItem)cbRace.SelectedItem;
            string race = racevalue.Content.ToString();

            switch (race)
            {
                case "Dwarf":
                    raceCON = 2;
                    txtFCON.Text = (int.Parse(txtFCON.Text) + raceCON).ToString();

                    lblSp15.Content = "Speed: 25";
                    lblSp16.Content = "Languages: Common, Dwarvish";

                    // lblSp18.Content = "Traits:";
                    lblSp18.Content = "Darkvision 60ft";
                    lblSp19.Content = "Dwarven Resilence";
                    lblSp20.Content = "Dwarven Combat training";

                    cbSub.ItemsSource = new string[] { "Choose!", "Hill Dwarf", "Mountain Dwarf" };
                    cbSub.SelectedIndex = 0;
                    break;
                    /*

                case "Elf":
                    raceDEX = 2;
                    txtFDEX.Text = (int.Parse(txtFDEX.Text) + raceDEX).ToString();
                    txtSpeed.Text = "30";
                    txtExtra.Text = "Darkvision 60ft, ";
                    txtExtra.Text = txtExtra.Text + "Fey Ancestry, ";
                    txtExtra.Text = txtExtra.Text + "Trance";
                    Perception.IsChecked = true;
                    txtLanguages.Text = txtLanguages.Text + ", Elvish";

                    cbSubrace.ItemsSource = new string[] { "Choose!", "High Elf", "Wood Elf", "Drow" };
                    cbSubrace.SelectedIndex = 0;
                    break;

                case "Halfling":
                    raceDEX = 2;
                    txtFDEX.Text = (int.Parse(txtFDEX.Text) + raceDEX).ToString();
                    txtSpeed.Text = "25";
                    txtExtra.Text = "Lucky, Brave, Halfling Nimbleness, Size: Small";
                    txtLanguages.Text = "Common, Halfling";

                    cbSubrace.ItemsSource = new string[] { "Choose!", "Lightfoot", "Stout" };
                    cbSubrace.SelectedIndex = 0;
                    break;*/
            }
        }

        // Subrace selection
        private void cbSub_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // lblSp 28-39
            for (int i = 28; i < 39; i++)
            {
                Label lb = (Label)FindName("lblSp" + i);
                lb.Content = "";
            }

        }
        #endregion

        //Free space


        //////////////////////////////////////////////////////////////////////////////
    } // Public partial class end
} // Namespace end