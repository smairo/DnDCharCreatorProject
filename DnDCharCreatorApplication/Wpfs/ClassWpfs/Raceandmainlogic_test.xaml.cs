﻿using DnDCharCreatorApplication.Classes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DnDCharCreatorApplication.Wpfs.ClassWpfs
{
    /// <summary>
    /// Interaction logic for Barbarian.xaml
    /// </summary>
    public partial class Barbarian : Window
    {
        private MyCharacter Character = new MyCharacter();

        private int ASRemaining, AScorecase;
        private int raceSTR, raceDEX, raceCON, raceINT, raceWIS, raceCHA,
            vraceSTR, vraceDEX, vraceCON, vraceINT, vraceWIS, vraceCHA = 0;

        private ObservableCollection<string> Subraces;

        public Barbarian()
        {
            InitializeComponent();

            // DO class adding stuff to Character class here
            Character.Class = "Barbarian";
        }

        private void btnRacedone_Click(object sender, RoutedEventArgs e)
        {
            //tab1.IsEnabled = false;
            //tab2.IsEnabled = true;
            //tcChartabs.SelectedIndex = tcChartabs.SelectedIndex + 1;
            RacialSetup(true);
            SubRacialSetup(true);
            stcVariant.Visibility = Visibility.Hidden;
            stcHalfElf.Visibility = Visibility.Hidden;
        }

        #region AbilityScore Controls
        // Minus logic
        #region Minus
        private void btnMSTR_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtSTR.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtSTR.Text = "8";
                    break;

                case 14:
                    txtSTR.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtSTR.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtSTR.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        private void btnMDEX_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtDEX.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtDEX.Text = "8";
                    break;

                case 14:
                    txtDEX.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtDEX.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtDEX.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        private void btnMCON_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtCON.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtCON.Text = "8";
                    break;

                case 14:
                    txtCON.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtCON.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtCON.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        private void btnMINT_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtINT.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtINT.Text = "8";
                    break;

                case 14:
                    txtINT.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtINT.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtINT.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        private void btnMWIS_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtWIS.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtWIS.Text = "8";
                    break;

                case 14:
                    txtWIS.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtWIS.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtWIS.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        private void txtFSTR_TextChanged(object sender, TextChangedEventArgs e)
        {
            int tmp = int.Parse(txtFSTR.Text);

            try
            {
                switch (tmp)
                {
                    case 8:
                    case 9:
                        lblSTRMod.Content = "-1";
                        break;

                    case 10:
                    case 11:
                        lblSTRMod.Content = " 0";
                        break;

                    case 12:
                    case 13:
                        lblSTRMod.Content = "+1";
                        break;

                    case 14:
                    case 15:
                        lblSTRMod.Content = "+2";
                        break;

                    case 16:
                    case 17:
                        lblSTRMod.Content = "+3";
                        break;

                    case 18:
                    case 19:
                        lblSTRMod.Content = "+4";
                        break;

                    case 20:
                        lblSTRMod.Content = "+5";
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void txtFDEX_TextChanged(object sender, TextChangedEventArgs e)
        {
            int tmp = int.Parse(txtFDEX.Text);
            try
            {
                switch (tmp)
                {
                    case 8:
                    case 9:
                        lblDEXMod.Content = "-1";
                        break;

                    case 10:
                    case 11:
                        lblDEXMod.Content = " 0";
                        break;

                    case 12:
                    case 13:
                        lblDEXMod.Content = "+1";
                        break;

                    case 14:
                    case 15:
                        lblDEXMod.Content = "+2";
                        break;

                    case 16:
                    case 17:
                        lblDEXMod.Content = "+3";
                        break;

                    case 18:
                    case 19:
                        lblDEXMod.Content = "+4";
                        break;

                    case 20:
                        lblDEXMod.Content = "+5";
                        break;
                }


            }
            catch (Exception)
            {

            }
        }

        private void txtFCON_TextChanged(object sender, TextChangedEventArgs e)
        {
            int tmp = int.Parse(txtFCON.Text);

            try
            {
                switch (tmp)
                {
                    case 8:
                    case 9:
                        lblCONMod.Content = "-1";
                        break;

                    case 10:
                    case 11:
                        lblCONMod.Content = " 0";
                        break;

                    case 12:
                    case 13:
                        lblCONMod.Content = "+1";
                        break;

                    case 14:
                    case 15:
                        lblCONMod.Content = "+2";
                        break;

                    case 16:
                    case 17:
                        lblCONMod.Content = "+3";
                        break;

                    case 18:
                    case 19:
                        lblCONMod.Content = "+4";
                        break;

                    case 20:
                        lblCONMod.Content = "+5";
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void txtFINT_TextChanged(object sender, TextChangedEventArgs e)
        {
            int tmp = int.Parse(txtFINT.Text);

            try
            {
                switch (tmp)
                {
                    case 8:
                    case 9:
                        lblINTMod.Content = "-1";
                        break;

                    case 10:
                    case 11:
                        lblINTMod.Content = " 0";
                        break;

                    case 12:
                    case 13:
                        lblINTMod.Content = "+1";
                        break;

                    case 14:
                    case 15:
                        lblINTMod.Content = "+2";
                        break;

                    case 16:
                    case 17:
                        lblINTMod.Content = "+3";
                        break;

                    case 18:
                    case 19:
                        lblINTMod.Content = "+4";
                        break;

                    case 20:
                        lblINTMod.Content = "+5";
                        break;
                }

            }
            catch (Exception)
            {
            }
        }

        private void txtFWIS_TextChanged(object sender, TextChangedEventArgs e)
        {
            int tmp = int.Parse(txtFWIS.Text);

            try
            {
                switch (tmp)
                {
                    case 8:
                    case 9:
                        lblWISMod.Content = "-1";
                        break;

                    case 10:
                    case 11:
                        lblWISMod.Content = " 0";
                        break;

                    case 12:
                    case 13:
                        lblWISMod.Content = "+1";
                        break;

                    case 14:
                    case 15:
                        lblWISMod.Content = "+2";
                        break;

                    case 16:
                    case 17:
                        lblWISMod.Content = "+3";
                        break;

                    case 18:
                    case 19:
                        lblWISMod.Content = "+4";
                        break;

                    case 20:
                        lblWISMod.Content = "+5";
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void txtFCHA_TextChanged(object sender, TextChangedEventArgs e)
        {
            int tmp = int.Parse(txtFCHA.Text);
            try
            {
                switch (tmp)
                {
                    case 8:
                    case 9:
                        lblCHAMod.Content = "-1";
                        break;

                    case 10:
                    case 11:
                        lblCHAMod.Content = " 0";
                        break;

                    case 12:
                    case 13:
                        lblCHAMod.Content = "+1";
                        break;

                    case 14:
                    case 15:
                        lblCHAMod.Content = "+2";
                        break;

                    case 16:
                    case 17:
                        lblCHAMod.Content = "+3";
                        break;

                    case 18:
                    case 19:
                        lblCHAMod.Content = "+4";
                        break;

                    case 20:
                        lblCHAMod.Content = "+5";
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnMCHA_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtCHA.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 8:
                    txtCHA.Text = "8";
                    break;

                case 14:
                    txtCHA.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                case 15:
                    txtCHA.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 2).ToString();
                    break;

                default:
                    txtCHA.Text = (AScorecase - 1).ToString();
                    txtPointsleft.Text = (ASRemaining + 1).ToString();
                    break;
            }
        }
        #endregion
        // Plus logic
        #region Plus
        private void btnPSTR_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtSTR.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 13:
                    if (ASRemaining > 1)
                    {
                        txtSTR.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtSTR.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtSTR.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtSTR.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }
        private void btnPDEX_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtDEX.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 13:
                    if (ASRemaining > 1)
                    {
                        txtDEX.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtDEX.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtDEX.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtDEX.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }

        private void btnPCON_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtCON.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 13:
                    if (ASRemaining > 1)
                    {
                        txtCON.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtCON.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtCON.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtCON.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }



        private void btnPINT_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtINT.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 13:
                    if (ASRemaining > 1)
                    {
                        txtINT.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtINT.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtINT.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtINT.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }

        private void btnPWIS_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtWIS.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 13:
                    if (ASRemaining > 1)
                    {
                        txtWIS.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtWIS.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtWIS.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtWIS.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }

        private void btnPCHA_Click(object sender, RoutedEventArgs e)
        {
            AScorecase = int.Parse(txtCHA.Text);
            ASRemaining = int.Parse(txtPointsleft.Text);
            switch (AScorecase)
            {
                case 13:
                    if (ASRemaining > 1)
                    {
                        txtCHA.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 14:
                    if (ASRemaining > 1)
                    {
                        txtCHA.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 2).ToString();
                    }
                    break;

                case 15:
                    txtCHA.Text = "15";
                    break;

                default:
                    if (ASRemaining > 0)
                    {
                        txtCHA.Text = (AScorecase + 1).ToString();
                        txtPointsleft.Text = (ASRemaining - 1).ToString();
                    }
                    break;
            }
        }


        #endregion
        // txt Change logic
        #region TextChanged event handler
        private void txtSTR_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFSTR.Text = (int.Parse(txtSTR.Text) + raceSTR + vraceSTR).ToString();
            }
            catch (Exception)
            {
            }
        }

        private void txtDEX_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFDEX.Text = (int.Parse(txtDEX.Text) + raceDEX + vraceDEX).ToString();
            }
            catch (Exception)
            {
            }
        }
        private void txtCON_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFCON.Text = (int.Parse(txtCON.Text) + raceCON + vraceCON).ToString();
            }
            catch (Exception)
            {
            }

        }
        private void txtINT_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFINT.Text = (int.Parse(txtINT.Text) + raceINT + vraceINT).ToString();
            }
            catch (Exception)
            {
            }
        }
        private void txtWIS_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFWIS.Text = (int.Parse(txtWIS.Text) + raceWIS + vraceWIS).ToString();
            }
            catch (Exception)
            {
            }
        }
        private void txtCHA_TextChanged(object sender, TextChangedEventArgs e)
        {
            try // txtF... is null when program is initialized so we need to catch this
            {
                txtFCHA.Text = (int.Parse(txtCHA.Text) + raceCHA + vraceCHA).ToString();
            }
            catch (Exception)
            {
            }
        }
        #endregion
        #endregion

        private void cbRace_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            makeTab1Reset();
            RacialSetup(false);

        }
        private void cbSubrace_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            vraceSTR = 0;
            vraceDEX = 0;
            vraceCON = 0;
            vraceINT = 0;
            vraceWIS = 0;
            vraceCHA = 0;

            SubRacialSetup(false);
        }

        private void makeTab1Reset()
        {
            raceSTR = 0;
            raceDEX = 0;
            raceCON = 0;
            raceINT = 0;
            raceWIS = 0;
            raceCHA = 0;

            vraceSTR = 0;
            vraceDEX = 0;
            vraceCON = 0;
            vraceINT = 0;
            vraceWIS = 0;
            vraceCHA = 0;

            txtFSTR.Text = txtSTR.Text;
            txtDEX.Text = txtDEX.Text;
            txtFCON.Text = txtCON.Text;
            txtFINT.Text = txtINT.Text;
            txtFWIS.Text = txtWIS.Text;
            txtFCHA.Text = txtCHA.Text;

            cbSub.ItemsSource = new string[] { "" };
            //cbSubrace.SelectedIndex = -1;
        }

        private void RacialSetup(bool tab1ready)
        {
            stcVariant.Visibility = Visibility.Hidden;
            stcHalfElf.Visibility = Visibility.Hidden;
            cbSp1.IsEnabled = true;
            cbSp2.IsEnabled = false;cbSp2.ItemsSource = new string[] { "" };
            cbSp3.IsEnabled = true;
            cbSp3.IsEnabled = false;cbSp4.ItemsSource = new string[] { "" };

            Subraces = new ObservableCollection<string>();

            ComboBoxItem racevalue = (ComboBoxItem)cbRace.SelectedItem;
            string race = racevalue.Content.ToString();

            switch (race)
            {
                case "Dwarf":
                    raceCON = 2;
                    txtFCON.Text = (int.Parse(txtFCON.Text) + raceCON).ToString();
                    txtSpeed.Text = "25";

                    if (tab1ready)
                    {
                        Character.Speed = 25;
                        txtLanguages.Text = "Common, Dwarvish";
                        Character.RaceSpecials.Add("Darkvision 60ft");
                        Character.RaceSpecials.Add("Dwarven Resilence");
                        Character.RaceSpecials.Add("Dwarven Combat training");
                    }
                    else
                    {
                        cbSub.ItemsSource = new string[] { "Choose!", "Hill Dwarf", "Mountain Dwarf" };
                        cbSub.SelectedIndex = 0;
                    }
                    break;

                // SUbrace in elf causes speed to appear bigger than it should bug
                case "Elf":
                    raceDEX = 2;
                    txtFDEX.Text = (int.Parse(txtFDEX.Text) + raceDEX).ToString();
                    txtSpeed.Text = "30";

                    if (tab1ready)
                    {
                        Character.Speed = 30;
                        txtLanguages.Text = "Common, Elvish";
                        Character.RaceSpecials.Add("Darkvision 60ft");
                        Character.RaceSpecials.Add("Fey Ancestry");
                        Character.RaceSpecials.Add("Trance");
                        Character.Skills.Add("Perception");
                    }
                    else
                    {
                        cbSub.ItemsSource = new string[] { "Choose!", "High Elf", "Wood Elf", "Drow" };
                        cbSub.SelectedIndex = 0;
                    }
                    break;

                case "Halfling":
                    raceDEX = 2;
                    txtFDEX.Text = (int.Parse(txtFDEX.Text) + raceDEX).ToString();
                    txtSpeed.Text = "25";

                    if (tab1ready)
                    {
                        Character.Speed = 25;
                        txtLanguages.Text = "Common, Halfling";
                        Character.RaceSpecials.Add("Lucky");
                        Character.RaceSpecials.Add("Brave");
                        Character.RaceSpecials.Add("Halfling Nimbleness");
                        Character.RaceSpecials.Add("Size: Small");
                    }
                    else
                    {
                        cbSub.ItemsSource = new string[] { "Choose!", "Lightfoot", "Stout" };
                        cbSub.SelectedIndex = 0;
                    }
                    break;

                case "Human":
                    txtSpeed.Text = "30";

                    if (tab1ready)
                    {
                        Character.Speed = 30;
                        txtLanguages.Text = "Common, EXTRA";
                    }
                    else
                    {
                        cbSub.ItemsSource = new string[] { "Normal", "Variant" };
                        cbSub.SelectedIndex = 0;
                    }
                    break;

                case "Dragonborn":
                    raceSTR = 2; raceCHA = 1;
                    txtFSTR.Text = (int.Parse(txtFSTR.Text) + raceSTR).ToString();
                    txtFCHA.Text = (int.Parse(txtFCHA.Text) + raceCHA).ToString();
                    txtSpeed.Text = "30";

                    if (tab1ready)
                    {
                        Character.Speed = 30;
                        txtLanguages.Text = "Common, Draconic";
                        Character.RaceSpecials.Add("Draconic Ancestry");
                    }
                    break;

                case "Gnome":
                    raceINT = 1;
                    txtFINT.Text = (int.Parse(txtFINT.Text) + raceINT).ToString();
                    txtSpeed.Text = "25";

                    if (tab1ready)
                    {
                        Character.Speed = 25;
                        txtLanguages.Text = "Common, Gnomish";
                        Character.RaceSpecials.Add("Size: Small");
                        Character.RaceSpecials.Add("Darkvision 60ft");
                        Character.RaceSpecials.Add("Gnome Cunning");
                    }
                    else
                    {
                        cbSub.ItemsSource = new string[] { "Choose", "Forest Gnome", "Rock Gnome" };
                        cbSub.SelectedIndex = 0;
                    }
                    break;

                case "Half-elf":
                    raceCHA = 2;
                    txtFCHA.Text = (int.Parse(txtFCHA.Text) + raceCHA).ToString();
                    txtSpeed.Text = "30";
                    stcHalfElf.Visibility = Visibility.Visible;

                    if (tab1ready)
                    {
                        Character.Speed = 30;
                        txtLanguages.Text = "Common, Elvish, EXTRA";
                        Character.RaceSpecials.Add("Darkvision 60ft");
                        Character.RaceSpecials.Add("Fey Ancestry");

                        ComboBoxItem HalfES1 = (ComboBoxItem)cbHalfElfSKills1.SelectedItem;
                        string helf1 = racevalue.Content.ToString();
                        ComboBoxItem HalfES2 = (ComboBoxItem)cbHalfElfSKills2.SelectedItem;
                        string helf2 = racevalue.Content.ToString();

                        Character.Skills.Add(helf1);
                        Character.Skills.Add(helf2);
                    }
                    else
                    {
                        //cbSub.ItemsSource = new string[] { "No subrace" };
                        //cbSub.SelectedIndex = 0;

                        cbSp3.ItemsSource = new string[] { "Choose +1 to ability", "STR", "DEX", "CON", "INT", "WIS", "CHA" };
                        cbSp3.SelectedIndex = 0;

                        cbSp4.SelectedIndex = -1;
                        cbSp4.IsEnabled = false;
                        cbSp3.IsEnabled = true;
                    }
                    break;

                case "Half-orc":
                    break;

                case "Tiefling":
                    break;

                case "Aasimar":
                    break;

                case "Eladrin":
                    break;

                case "Changeling":
                    break;

                case "Shifter":
                    break;

                case "Warforged":
                    break;

                case "Aarakocra":
                    break;

                case "Genasi":
                    break;

                case "Goliath":
                    break;

                case "Minotaur":
                    break;

            }
        }

        private void SubRacialSetup(bool tab1ready)
        {
            vraceSTR = 0;
            vraceDEX = 0;
            vraceCON = 0;
            vraceINT = 0;
            vraceWIS = 0;
            vraceCHA = 0;

            txtFSTR.Text = (int.Parse(txtSTR.Text) + raceSTR).ToString();
            txtFDEX.Text = (int.Parse(txtDEX.Text) + raceDEX).ToString();
            txtFCON.Text = (int.Parse(txtCON.Text) + raceCON).ToString();
            txtFINT.Text = (int.Parse(txtINT.Text) + raceINT).ToString();
            txtFWIS.Text = (int.Parse(txtWIS.Text) + raceWIS).ToString();
            txtFCHA.Text = (int.Parse(txtCHA.Text) + raceCHA).ToString();

            string subrace = (string)(cbSub.SelectedItem);


            switch (subrace)
            {
                case "Hill Dwarf":
                    vraceWIS = 1;
                    txtFWIS.Text = (int.Parse(txtFWIS.Text) + vraceWIS).ToString();
                    //Character.SubRaceSpecials.Add("Dwarven toughness");
                    break;

                case "Mountain Dwarf":
                    vraceSTR = 2;
                    txtFSTR.Text = (int.Parse(txtFSTR.Text) + vraceSTR).ToString();
                    //Character.ArmorProfiencies.Add("Light");
                    //Character.ArmorProfiencies.Add("Medium");
                    break;

                case "High Elf":
                    vraceINT = 1;
                    txtFINT.Text = (int.Parse(txtFINT.Text) + vraceINT).ToString();
                    //Character.WeaponProfs.Add("Longsword");
                    //Character.WeaponProfs.Add("Shortsword");
                    //Character.WeaponProfs.Add("Shortbow");
                    //Character.WeaponProfs.Add("Longbow");
                    //Character.Languages.Add("EXTRA!");
                    //Character.SubRaceSpecials.Add("Any one Wizard cantrip while Intelligence as spellcasting ability for it.");
                    break;

                case "Wood Elf":
                    vraceWIS = 1;
                    txtFWIS.Text = (int.Parse(txtFWIS.Text) + vraceWIS).ToString();
                    txtSpeed.Text = "35";
                    //Character.WeaponProfs.Add("Longsword");
                    //Character.WeaponProfs.Add("Shortsword");
                    //Character.WeaponProfs.Add("Shortbow");
                    //Character.WeaponProfs.Add("Longbow");
                    //Character.SubRaceSpecials.Add("Mask of the wild");
                    break;

                case "Drow":
                    vraceCHA = 1;
                    txtFCHA.Text = (int.Parse(txtFCHA.Text) + vraceCHA).ToString();
                    //Character.WeaponProfs.Add("Rapiers");
                    //Character.WeaponProfs.Add("shortswords");
                    //Character.WeaponProfs.Add("hand crossbows");
                    //Character.SubRaceSpecials.Add("Darkvision 120ft, Sunlight Sensivity, Drow Magic");
                    break;

                case "Lightfoot":
                    vraceCHA = 1;
                    txtFCHA.Text = (int.Parse(txtFCHA.Text) + vraceCHA).ToString();
                    //Character.SubRaceSpecials.Add("Naturally stealthy");
                    break;

                case "Stout":
                    vraceCON = 1;
                    txtFCON.Text = (int.Parse(txtFCON.Text) + vraceCON).ToString();
                    //Character.SubRaceSpecials.Add("Stout Resilence");
                    break;

                case "Normal":
                    stcVariant.Visibility = Visibility.Hidden;
                    raceCHA = 1; raceCON = 1; raceDEX = 1; raceINT = 1; raceSTR = 1; raceWIS = 1;
                    txtFCHA.Text = (int.Parse(txtFCHA.Text) + raceCHA).ToString(); txtFCON.Text = (int.Parse(txtFCON.Text) + raceCON).ToString();
                    txtFDEX.Text = (int.Parse(txtFDEX.Text) + raceDEX).ToString(); txtFINT.Text = (int.Parse(txtFINT.Text) + raceINT).ToString();
                    txtFSTR.Text = (int.Parse(txtFSTR.Text) + raceSTR).ToString(); txtFWIS.Text = (int.Parse(txtFWIS.Text) + raceWIS).ToString();
                    break;

                case "Variant":
                    // To function this or not to function this?
                    raceCHA = 0; raceCON = 0; raceDEX = 0; raceINT = 0; raceSTR = 0; raceWIS = 0;
                    txtFCHA.Text = txtCHA.Text; txtFCON.Text = txtCON.Text;
                    txtFDEX.Text = txtDEX.Text; txtFINT.Text = txtINT.Text;
                    txtFSTR.Text = txtSTR.Text; txtFWIS.Text = txtWIS.Text;
                    stcVariant.Visibility = Visibility.Visible;

                    if (tab1ready)
                    {
                        Character.SubRaceSpecials.Add("Any feat");
                    }
                    else
                    {
                        cbSp1.ItemsSource = new string[] { "Choose +1 to ability", "STR", "DEX", "CON", "INT", "WIS", "CHA" };
                        cbSp1.SelectedIndex = 0;
                        
                        cbSp2.SelectedIndex = -1;
                        cbSp2.IsEnabled = false;
                        cbSp1.IsEnabled = true;
                    }
                    break;

                case "Forest Gnome":
                    vraceDEX = 1;
                    txtFDEX.Text = (int.Parse(txtFDEX.Text) + vraceDEX).ToString();

                    if (tab1ready)
                    {
                        Character.SubRaceSpecials.Add("Minor illusion using INT");
                        Character.SubRaceSpecials.Add("Speak with small Beasts");
                    }
                    break;

                case "Rock Gnome":

                    vraceCON = 1;
                    txtFCON.Text = (int.Parse(txtFCON.Text) + vraceCON).ToString();

                    if (tab1ready)
                    {
                        Character.SubRaceSpecials.Add("Artificer's Lore");
                        Character.SubRaceSpecials.Add("Tinker");
                    }
                    break;

                case "Eladrin":
                    break;

                case "Changeling":
                    break;

                case "Shifter":
                    break;

                case "Warforged":
                    break;

                case "Aarakocra":
                    break;

                case "Genasi":
                    break;

                case "Goliath":
                    break;

                case "Minotaur":
                    break;

            }
        }

        private void cbSp1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (stcVariant.Visibility == Visibility.Visible)
            {
                string selabi = (string)(cbSp1.SelectedItem);

                switch (selabi)
                {
                    case "STR":
                        vraceSTR = 1;
                        txtFSTR.Text = (int.Parse(txtFSTR.Text) + 1).ToString();
                        cbSp1.IsEnabled = false;
                        break;
                    case "DEX":
                        vraceDEX = 1;
                        txtFDEX.Text = (int.Parse(txtFDEX.Text) + 1).ToString();
                        cbSp1.IsEnabled = false;
                        break;
                    case "CON":
                        vraceCON = 1;
                        txtFCON.Text = (int.Parse(txtFCON.Text) + 1).ToString();
                        cbSp1.IsEnabled = false;
                        break;
                    case "INT":
                        vraceINT = 1;
                        txtFINT.Text = (int.Parse(txtFINT.Text) + 1).ToString();
                        cbSp1.IsEnabled = false;
                        break;
                    case "WIS":
                        vraceWIS = 1;
                        txtFWIS.Text = (int.Parse(txtFWIS.Text) + 1).ToString();
                        cbSp1.IsEnabled = false;
                        break;
                    case "CHA":
                        vraceCHA = 1;
                        txtFCHA.Text = (int.Parse(txtFCHA.Text) + 1).ToString();
                        cbSp1.IsEnabled = false;
                        break;
                }


                List<string> abilist = new List<string>();
                abilist.Add("Choose +1 to ability");
                string[] growabi;
                string[] abilities = { "STR", "DEX", "CON", "INT", "WIS", "CHA" };
                foreach (string value in abilities)
                {
                    if (value != selabi)
                    {
                        abilist.Add(value);
                    }
                }
                growabi = abilist.ToArray();

                cbSp2.ItemsSource = growabi;

                cbSp2.IsEnabled = true;
                //cbSp1.IsEnabled = false;
            }
        }

        private void cbSp2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //cbSp1.IsEnabled = false;

            if (stcVariant.Visibility == Visibility.Visible)
            {
                string selabi = (string)(cbSp2.SelectedItem);

                switch (selabi)
                {
                    case "STR":
                        vraceSTR = 1;
                        txtFSTR.Text = (int.Parse(txtFSTR.Text) + 1).ToString();
                        cbSp2.IsEnabled = false;
                        break;
                    case "DEX":
                        vraceDEX = 1;
                        txtFDEX.Text = (int.Parse(txtFDEX.Text) + 1).ToString(); cbSp2.IsEnabled = false;
                        break;
                    case "CON":
                        vraceCON = 1;
                        txtFCON.Text = (int.Parse(txtFCON.Text) + 1).ToString(); cbSp2.IsEnabled = false;
                        break;
                    case "INT":
                        vraceINT = 1;
                        txtFINT.Text = (int.Parse(txtFINT.Text) + 1).ToString(); cbSp2.IsEnabled = false;
                        break;
                    case "WIS":
                        vraceWIS = 1;
                        txtFWIS.Text = (int.Parse(txtFWIS.Text) + 1).ToString(); cbSp2.IsEnabled = false;
                        break;
                    case "CHA":
                        vraceCHA = 1;
                        txtFCHA.Text = (int.Parse(txtFCHA.Text) + 1).ToString(); cbSp2.IsEnabled = false;
                        break;
                }
                //cbSp2.IsEnabled = true;


            }
        }
        private void cbSp3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (stcHalfElf.Visibility == Visibility.Visible)
            {
                string selabi = (string)(cbSp3.SelectedItem);

                switch (selabi)
                {
                    case "STR":
                        vraceSTR = 1;
                        txtFSTR.Text = (int.Parse(txtFSTR.Text) + 1).ToString();
                        cbSp3.IsEnabled = false;
                        break;
                    case "DEX":
                        vraceDEX = 1;
                        txtFDEX.Text = (int.Parse(txtFDEX.Text) + 1).ToString();
                        cbSp3.IsEnabled = false;
                        break;
                    case "CON":
                        vraceCON = 1;
                        txtFCON.Text = (int.Parse(txtFCON.Text) + 1).ToString();
                        cbSp3.IsEnabled = false;
                        break;
                    case "INT":
                        vraceINT = 1;
                        txtFINT.Text = (int.Parse(txtFINT.Text) + 1).ToString();
                        cbSp3.IsEnabled = false;
                        break;
                    case "WIS":
                        vraceWIS = 1;
                        txtFWIS.Text = (int.Parse(txtFWIS.Text) + 1).ToString();
                        cbSp3.IsEnabled = false;
                        break;
                    case "CHA":
                        vraceCHA = 1;
                        txtFCHA.Text = (int.Parse(txtFCHA.Text) + 1).ToString();
                        cbSp3.IsEnabled = false;
                        break;
                }


                List<string> abilist = new List<string>();
                abilist.Add("Choose +1 to ability");
                string[] growabi;
                string[] abilities = { "STR", "DEX", "CON", "INT", "WIS", "CHA" };
                foreach (string value in abilities)
                {
                    if (value != selabi)
                    {
                        abilist.Add(value);
                    }
                }
                growabi = abilist.ToArray();

                cbSp4.ItemsSource = growabi;
                cbSp4.IsEnabled = true;
                //cbSp1.IsEnabled = false;
            }
        }

        private void cbSp4_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //cbSp1.IsEnabled = false;

            if (stcHalfElf.Visibility == Visibility.Visible)
            {
                string selabi = (string)(cbSp4.SelectedItem);

                switch (selabi)
                {
                    case "STR":
                        vraceSTR = 1;
                        txtFSTR.Text = (int.Parse(txtFSTR.Text) + 1).ToString();
                        cbSp4.IsEnabled = false;
                        break;
                    case "DEX":
                        vraceDEX = 1;
                        txtFDEX.Text = (int.Parse(txtFDEX.Text) + 1).ToString();
                        cbSp4.IsEnabled = false;
                        break;
                    case "CON":
                        vraceCON = 1;
                        txtFCON.Text = (int.Parse(txtFCON.Text) + 1).ToString();
                        cbSp4.IsEnabled = false;
                        break;
                    case "INT":
                        vraceINT = 1;
                        txtFINT.Text = (int.Parse(txtFINT.Text) + 1).ToString();
                        cbSp4.IsEnabled = false;
                        break;
                    case "WIS":
                        vraceWIS = 1;
                        txtFWIS.Text = (int.Parse(txtFWIS.Text) + 1).ToString();
                        cbSp4.IsEnabled = false;
                        break;
                    case "CHA":
                        vraceCHA = 1;
                        txtFCHA.Text = (int.Parse(txtFCHA.Text) + 1).ToString();
                        cbSp4.IsEnabled = false;
                        break;
                }

                //cbSp4.IsEnabled = false;
            }
        }
    }
}