﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DnDCharCreatorApplication.Wpfs.ClassWpfs
{
    /// <summary>
    /// Interaction logic for Background_test.xaml
    /// </summary>
    public partial class Background_test : Window
    {
        public Background_test()
        {
            InitializeComponent();
        }

        // Use stcSkillsControls to loop through all checkboxes if you want reset all if comboboxes are manipulated!
        // http://stackoverflow.com/questions/16920027/looping-over-xaml-defined-labels
        // Just use control.isChecked = false; inside that loop

        /*  Get combobox item value and do stuff with it

            ComboBoxItem racevalue = (ComboBoxItem)cbRace.SelectedItem;
            string race = racevalue.Content.ToString();

            switch (race)
            {
            case "Human":
                // Do things
                break;
            }
        */
    }
}
