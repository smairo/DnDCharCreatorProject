﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnDCharCreatorApplication.Classes
{
    public class MyCharacter
    {
        // Constractÿýr
        public MyCharacter()
        {
         ArmorProfiencies = new List<string>();

         Feats = new List<string>();

         ClassSpecials = new List<string>();
         RaceSpecials = new List<string>();
         SubRaceSpecials = new List<string>();

         Languages = new List<string>();
         ProfientSkills = new List<string>();
         ProfientTools = new List<string>();
         Saving_throws = new List<string>();

         Weapons = new List<string>();
         WeaponProfs = new List<string>();
         Spells = new List<int>();

         EquipmentPacks = new List<string>();
         Equipment = new List<string>();
         Skills = new List<string>();
        }

        public string Name { get; set; } // tab1
        public string Race { get; set; } // tab1
        public string Class { get; set; } // Initialize
        public string Gender { get; set; }
        public string Subclass { get; set; } // tab1
        public string Aligment { get; set; }
        public string Background { get; set; }

        public int AC { get; set; }
        public int HitPoints { get; set; }
        public string Hitdice { get; set; }
        public int Initiative { get; set; }
        public int Speed { get; set; }
        public int Profiency { get; set; }
        public int Level { get; set; }
        public float Experience { get; set; }
        public int GoldPieces { get; set; }

        // All in tab1
        public int Str { get; set; }
        public int Dex { get; set; }
        public int Con { get; set; }
        public int Int { get; set; }
        public int Wis { get; set; }
        public int Cha { get; set; }

        public List<string> Skills { get; set; }

        public List<string> ArmorProfiencies { get; set; }

        public List<string> Feats { get; set; }

        public List<string> ClassSpecials { get; set; }
        public List<string> RaceSpecials { get; set; }
        public List<string> SubRaceSpecials { get; set; }

        public List<string> Languages { get; set; }
        public List<string> ProfientSkills { get; set; }
        public List<string> ProfientTools { get; set; }
        public List<string> Saving_throws { get; set; }

        public List<string> Weapons { get; set; }
        public List<string> WeaponProfs { get; set; }
        public List<int> Spells { get; set; }

        public List<string> EquipmentPacks { get; set; }
        public List<string> Equipment { get; set; }

        public int Height { get; set; }
        public int Weight { get; set; }
        public int Age { get; set; }
        public string Personality { get; set; }
        public string Ideals { get; set; }
        public string Flaws { get; set; }
        public string Bonds { get; set; }
        public string Backstory { get; set; }

        public string Playername { get; set; }

        // Not needed?
        // public int Ammo { get; set; }
    }
}
